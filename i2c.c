/*
 * i2c.c
 *
 *  Created on: 2014?3?29?
 *      Author: FrankZhang
 */
#include "i2c.h"


void (*user_onRequest)(void);
void (*user_onReceive)(int);


void I2C1_EV_IRQHandler(){
	Wire_interruptHandler_EV();
}

void I2C1_ER_IRQHandler(){
	Wire_interruptHandler_ER();
}

void Wire_onReceive( void (*function)(int) )
{
  user_onReceive = function;
}

// sets function called on slave read
void Wire_onRequest( void (*function)(void) )
{
  user_onRequest = function;
}

void Wire_onReceiveService(int numBytes)
{
  // don't bother if user hasn't registered a callback
  if(!user_onReceive){
    return;
  }
  user_onReceive(numBytes);
}

void Wire_onRequestService(void)
{
  // don't bother if user hasn't registered a callback
  if(!user_onRequest){
    return;
  }
  user_onRequest();
}


void Wire_pullDownSCL(I2C_TypeDef* I2Cx){
	if (I2Cx == I2C1){
		GPIO_InitTypeDef  GPIO_InitStructure;
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
		GPIO_ResetBits(GPIOB, GPIO_Pin_6);
	}
}

void Wire_releaseSCL(I2C_TypeDef* I2Cx){
	if (I2Cx == I2C1){
		GPIO_InitTypeDef  GPIO_InitStructure;
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
	}
}

void Wire_interruptHandler_ER(){
	if (I2C_GetFlagStatus(I2C1,I2C_FLAG_AF)){
		//check if is sending data (slave transmitter)
		if ((i2cMsg.mode==I2C_S_WRITE)&&(i2cStatus==I2C_STATUS_BUSY)){
			//clear AF by writing to AF
			//change mode since STOPF will not be set
			I2C_ClearFlag(I2C1,I2C_FLAG_AF);
			i2cMsg.mode=I2C_S_READ;
			i2cStatus = I2C_STATUS_IDLE;
		}
	}
}

void Wire_interruptHandler_EV(){
	if ((i2cMsg.mode==I2C_M_READ)||(i2cMsg.mode==I2C_M_WRITE)){
		/*
		 * Master
		 */
		if (i2cMsg.mode==I2C_M_WRITE){
			/*
			 * Write
			 */
			if (I2C_GetFlagStatus(I2C1,I2C_FLAG_SB)){
				//START has been generated
				//Write ADDR+W to DR => Clear SB
				I2C_Send7bitAddress(I2C1,i2cMsg.addr,I2C_Direction_Transmitter);
				i2cMsg.xferred = 0;
				return;
			}

			if (I2C_GetFlagStatus(I2C1,I2C_FLAG_ADDR)){
				//ADDR set, slave has ACK
				//clear ADDR
				i2cStatus = I2C_STATUS_BUSY;
				((void)(I2C1->SR1));
				((void)(I2C1->SR2));
				//write first data
				I2C_SendData(I2C1,i2cMsg.data[i2cMsg.xferred]);
				i2cMsg.xferred++;
				return;
			}
			if (I2C_GetFlagStatus(I2C1,I2C_FLAG_BTF)){
				//byte has been transferred
				if (i2cMsg.length > i2cMsg.xferred){
					I2C_SendData(I2C1,i2cMsg.data[i2cMsg.xferred]);
					i2cMsg.xferred++;
					return;
				}
				if (i2cMsg.length == i2cMsg.xferred){
					//all sent
					//set STOP
					I2C_GenerateSTOP(I2C1,ENABLE);
					i2cStatus = I2C_STATUS_XFER_DONE;
					return;
					//::TODO allow repeated START =>
				}
			}
			if ((I2C_GetFlagStatus(I2C1,I2C_FLAG_STOPF))&&(!(I2C_GetFlagStatus(I2C1,I2C_FLAG_MSL)))){
				//unexpected STOP on bus when endTransmission has been called
				GPIO_SetBits(BLINK_PORT,BLINK_PIN);
				Wire_softwareReset();
				GPIO_ResetBits(BLINK_PORT,BLINK_PIN);
				I2C_GenerateSTART(I2C1,ENABLE);			//use Firmware function to prevent crash
				return;
			}
		} else {
			/*
			 * Read - the most shitty part
			 */
			if (I2C_GetFlagStatus(I2C1,I2C_FLAG_SB)){
				//START has been generated
				//write ADDR+W to DR => Clear SB
				I2C_Send7bitAddress(I2C1,i2cMsg.addr,I2C_Direction_Receiver);
				i2cMsg.xferred = 0;
				if (i2cMsg.length == 2){
					//set POS and ACK before ADDR is set
					I2C_NACKPositionConfig(I2C1,I2C_NACKPosition_Next);
					I2C_AcknowledgeConfig(I2C1,ENABLE);
				}
				return;
			}
			// FIRST determine how many bytes
			if (i2cMsg.length == 1){
				if (I2C_GetFlagStatus(I2C1,I2C_FLAG_ADDR)){
					Wire_pullDownSCL(I2C1);
					i2cStatus = I2C_STATUS_BUSY;
					//clear ACK
					I2C_AcknowledgeConfig(I2C1,DISABLE);
					//clear ADDR
					((void)(I2C1->SR1));
					((void)(I2C1->SR2));
					I2C_GenerateSTOP(I2C1,ENABLE);
					Wire_releaseSCL(I2C1);
					return;
				}
				if (I2C_GetFlagStatus(I2C1,I2C_FLAG_RXNE)){
					i2cMsg.data[i2cMsg.xferred] = I2C_ReceiveData(I2C1);
					i2cMsg.xferred++;
					i2cStatus = I2C_STATUS_XFER_DONE;
					return;
				}
			} else if (i2cMsg.length == 2){
				if (I2C_GetFlagStatus(I2C1,I2C_FLAG_ADDR)){
					Wire_pullDownSCL(I2C1);
					i2cStatus = I2C_STATUS_BUSY;
					//clear ADDR
					((void)(I2C1->SR1));
					((void)(I2C1->SR2));
					//clear ACK
					I2C_AcknowledgeConfig(I2C1,DISABLE);
					Wire_releaseSCL(I2C1);
					return;
				}
				if (I2C_GetFlagStatus(I2C1,I2C_FLAG_BTF)){
					Wire_pullDownSCL(I2C1);
					//set STOP
					I2C_GenerateSTOP(I2C1,ENABLE);
					//read from DR
					i2cMsg.data[i2cMsg.xferred] = I2C_ReceiveData(I2C1);
					i2cMsg.xferred++;
					//release SCL
					Wire_releaseSCL(I2C1);
					//read again
					i2cMsg.data[i2cMsg.xferred] = I2C_ReceiveData(I2C1);
					i2cMsg.xferred++;
					i2cStatus = I2C_STATUS_XFER_DONE;
					return;
				}
			} else if (i2cMsg.length >= 3){
				if (I2C_GetFlagStatus(I2C1,I2C_FLAG_ADDR)){
					//clear ADDR
					i2cStatus = I2C_STATUS_BUSY;
					I2C_AcknowledgeConfig(I2C1,ENABLE);
					((void)(I2C1->SR1));
					((void)(I2C1->SR2));
					return;
				}
				if (I2C_GetFlagStatus(I2C1,I2C_FLAG_BTF)){
					if (i2cMsg.xferred == i2cMsg.length - 3){
						//clear ACK
						I2C_AcknowledgeConfig(I2C1,DISABLE);
						i2cMsg.data[i2cMsg.xferred] = I2C_ReceiveData(I2C1);
						i2cMsg.xferred++;
						return;
					} else if (i2cMsg.xferred == i2cMsg.length - 2){
						Wire_pullDownSCL(I2C1);
						I2C_GenerateSTOP(I2C1,ENABLE);
						i2cMsg.data[i2cMsg.xferred] = I2C_ReceiveData(I2C1);
						i2cMsg.xferred++;
						Wire_releaseSCL(I2C1);
						i2cMsg.data[i2cMsg.xferred] = I2C_ReceiveData(I2C1);
						i2cMsg.xferred++;
						i2cStatus = I2C_STATUS_XFER_DONE;
						return;
					} else {
						i2cMsg.data[i2cMsg.xferred] = I2C_ReceiveData(I2C1);
						i2cMsg.xferred++;
						return;
					}
				}
			}
			if ((I2C_GetFlagStatus(I2C1,I2C_FLAG_STOPF))&&(!(I2C_GetFlagStatus(I2C1,I2C_FLAG_MSL)))){
				//unexpected STOP on bus when endTransmission has been called
				GPIO_SetBits(BLINK_PORT,BLINK_PIN);
				Wire_softwareReset();
				GPIO_ResetBits(BLINK_PORT,BLINK_PIN);
				if (i2cMsg.length>0){						//there is something not sent
					I2C_GenerateSTART(I2C1,ENABLE);			//use Firmware function to prevent crash
				}
				return;
			}
		}
	} else {
		/*
		 * Slave
		 */

		if (I2C_GetFlagStatus(I2C1,I2C_FLAG_ADDR)){
			if (I2C_GetFlagStatus(I2C1,I2C_FLAG_TRA)){
				//trasmitter
				i2cMsg.mode = I2C_S_WRITE;
				i2cMsg.xferred = 0;
				i2cMsg.length = 0;
				i2cStatus = I2C_STATUS_BUSY;
				tx_buf_idx = 0;
			    i2cMsg.data = &tx_buf[tx_buf_idx];
				Wire_onRequestService();

				((void)(I2C1->SR1));
				((void)(I2C1->SR2));
				if (i2cMsg.length > i2cMsg.xferred){
					I2C_SendData(I2C1,i2cMsg.data[i2cMsg.xferred]);
					i2cMsg.xferred++;
				} else {
					I2C_SendData(I2C1,0xFF);
				}
				return;
			} else {
				//receiver
				i2cMsg.mode = I2C_S_READ;
				i2cMsg.xferred = 0;
				i2cMsg.data = &rx_buf[rx_buf_idx];
				i2cStatus = I2C_STATUS_BUSY;
				((void)(I2C1->SR1));
				((void)(I2C1->SR2));
				return;
			}
		}

		if (I2C_GetFlagStatus(I2C1,I2C_FLAG_BTF)){
			if (i2cMsg.mode == I2C_S_WRITE){
				if (i2cMsg.length > i2cMsg.xferred){
					I2C_SendData(I2C1,i2cMsg.data[i2cMsg.xferred]);
					i2cMsg.xferred++;
					return;
				} else {
					I2C_SendData(I2C1,0xFF);
					return;
				}
			}
		}

		if (I2C_GetFlagStatus(I2C1,I2C_FLAG_RXNE)){
			if (i2cMsg.mode == I2C_S_READ){
				//clear RxNE by reading from DR
				i2cMsg.data[i2cMsg.xferred] = I2C_ReceiveData(I2C1);
				i2cMsg.xferred++;
				return;
			}
		}

		if (I2C_GetFlagStatus(I2C1,I2C_FLAG_STOPF)){
			//printf("Slave STOPF");
			if (i2cMsg.mode==I2C_S_READ){
			    rx_buf_len += i2cMsg.xferred;
				Wire_onReceiveService(i2cMsg.xferred);
			}
			i2cStatus=I2C_STATUS_XFER_DONE;
			i2cMsg.mode=I2C_S_READ;
			i2cStatus = I2C_STATUS_IDLE;
			((void)(I2C1->SR1));
			I2C_Cmd(I2C1,ENABLE);
			return;
		}

	}
}


void Wire_begin(uint8_t addr){
	tx_buf_idx = 0;
    tx_buf_overflow = false;
    rx_buf_idx = 0;
    rx_buf_len = 0;
    i2cAddress = addr;
    Wire_interfaceInit(addr);
    i2cMsg.mode=I2C_S_READ;
    i2cStatus = I2C_STATUS_IDLE;
}

void Wire_interfaceInit(uint8_t addr){
	GPIO_InitTypeDef	GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_Init(GPIOB , &GPIO_InitStructure);

	I2C_InitTypeDef I2C_InitStructure;
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = i2cAddress;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = 100000;
	I2C_Init(I2C1,&I2C_InitStructure);

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = I2C1_EV_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = I2C1_ER_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	I2C_ITConfig(I2C1,I2C_IT_EVT|I2C_IT_BUF|I2C_IT_ERR,ENABLE);
}

void Wire_softwareReset(){
	I2C_ITConfig(I2C1,I2C_IT_EVT|I2C_IT_BUF|I2C_IT_ERR,DISABLE);
	I2C_SoftwareResetCmd(I2C1,ENABLE);
	I2C_SoftwareResetCmd(I2C1,DISABLE);
	Wire_interfaceInit(i2cAddress);
	I2C_ITConfig(I2C1,I2C_IT_EVT|I2C_IT_BUF|I2C_IT_ERR,ENABLE);
}

void Wire_generateStart(){
	uint16_t Timeout = 1024;
	I2C_GenerateSTART(I2C1,ENABLE);
	while (i2cStatus != I2C_STATUS_BUSY){
		Timeout --;
		if(Timeout == 0){
			GPIO_SetBits(BLINK_PORT,BLINK_PIN);
			//software reset sequence
			Wire_softwareReset();
			I2C_GenerateSTART(I2C1,ENABLE);
			GPIO_ResetBits(BLINK_PORT,BLINK_PIN);
			return;
		}
	}
}

void Wire_process(){
	if ((i2cMsg.mode==I2C_M_READ)||(i2cMsg.mode==I2C_M_WRITE)){
		i2cStatus = I2C_STATUS_START_SET;
		Wire_generateStart();
		if (i2cStatus == I2C_STATUS_START_SET){
			i2cStatus = I2C_STATUS_IDLE;
			Wire_softwareReset();
			return;
		}
  		while (i2cStatus != I2C_STATUS_XFER_DONE);
		while (I2C_GetFlagStatus(I2C1,I2C_FLAG_MSL));	//wait until STOP is sent
		i2cStatus = I2C_STATUS_IDLE;
	}

}


uint8_t Wire_available() {
    return rx_buf_len - rx_buf_idx;
}

uint8_t Wire_requestFrom(uint8_t slave_addr, uint16_t num){
	if (num > BUFSIZE) {
		num = BUFSIZE;
	}
	i2cMsg.addr = slave_addr;
	i2cMsg.mode = I2C_M_READ;
	i2cMsg.length = num;
	i2cMsg.data = &rx_buf[rx_buf_idx];

	Wire_process();

    rx_buf_len += i2cMsg.xferred;
    i2cMsg.mode = I2C_S_READ;
	return rx_buf_len;
}

void Wire_beginTransmission(uint8_t slave_addr){
	//master mode is selected
    i2cMsg.addr = slave_addr;
    i2cMsg.data = &tx_buf[tx_buf_idx];
    i2cMsg.length = 0;
}

void Wire_endTransmission(){
	i2cMsg.mode = I2C_M_WRITE;
    Wire_process();
    tx_buf_idx = 0;
    tx_buf_overflow = false;
    i2cMsg.mode = I2C_S_READ;
    //return ERROR
}

uint8_t Wire_receive() {
    if (rx_buf_idx == rx_buf_len) {
        rx_buf_idx = 0;
        rx_buf_len = 0;
        return 0;
    } else if (rx_buf_idx == (rx_buf_len-1)) {
    	uint8_t temp = rx_buf[rx_buf_idx];
        rx_buf_idx = 0;
        rx_buf_len = 0;
        return temp;
    }
    return rx_buf[rx_buf_idx++];
}

void Wire_send(uint8_t value) {
    if (tx_buf_idx == BUFSIZE) {
        tx_buf_overflow = true;
        return;
    }
    tx_buf[tx_buf_idx++] = value;
    i2cMsg.length++;
}

void Wire_sendArray(uint8_t* buf, int len) {
    for (uint8_t i = 0; i < len; i++) {
        Wire_send(buf[i]);
    }
}

void Wire_sendString(char* buf) {
    uint8_t *ptr = (uint8_t*)buf;
    while (*ptr) {
    	Wire_send(*ptr);
        ptr++;
    }
}

