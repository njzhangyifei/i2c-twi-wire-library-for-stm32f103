/*
 * i2c.h
 *
 *  Created on: 2014?3?29?
 *      Author: FrankZhang
 */

#ifndef I2C_H_
#define I2C_H_


#include "stm32f10x.h"
#include "macro.h"
#include "systime.h"

#define BUFSIZE 32

typedef struct i2cMsg{
	uint8_t addr;                /**< Address */

	#define I2C_S_READ			0x00
	#define I2C_S_WRITE			0x01
	#define I2C_M_READ			0x02
	#define I2C_M_WRITE			0x03

	uint8_t mode;


	uint16_t length;
	uint16_t xferred;
	uint8_t  *data;
} I2CMsg;

I2CMsg	i2cMsg;



typedef enum i2cStatus {
	I2C_STATUS_IDLE			= 0x00, /**< Disabled */
	I2C_STATUS_BUSY			= 0x01, /**< Idle */
	I2C_STATUS_XFER_DONE    = 0x02,
	I2C_STATUS_START_SET	= 0x03,
	I2C_STATUS_ERROR		= 0x04
} I2CStatus;

I2CStatus i2cStatus;   /**< Device state */
uint8_t i2cAddress;

uint8_t rx_buf[BUFSIZE];      /* receive buffer */
uint8_t rx_buf_idx;               /* first unread idx in rx_buf */
uint8_t rx_buf_len;               /* number of bytes read */
uint8_t tx_buf[BUFSIZE];      /* transmit buffer */
uint8_t tx_buf_idx;          // next idx available in tx_buf, -1 overflow
bool tx_buf_overflow;

void Wire_interfaceInit(uint8_t addr);
void Wire_softwareReset();
void Wire_generateStart();

uint8_t Wire_available();

void Wire_begin(uint8_t addr);
uint8_t Wire_requestFrom(uint8_t addr, uint16_t num);
void Wire_beginTransmission(uint8_t slave_addr);
void Wire_process();
void Wire_endTransmission();

uint8_t Wire_receive();
void Wire_sendString(char* buf);
void Wire_sendArray(uint8_t* buf, int len);
void Wire_send(uint8_t value);

void Wire_interruptHandler_ER();
void Wire_interruptHandler_EV();
void Wire_onRequest(void (*function)(void));
void Wire_onReceive(void (*function)(int));


#endif /* I2C_H_ */
